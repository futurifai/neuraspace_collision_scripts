import os
import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.metrics import mean_absolute_error

import tensorflow_probability as tfp
import tensorflow as tf 
tfd = tfp.distributions
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, BatchNormalization
from tensorflow.keras.layers import ReLU, Dropout
from tensorflow.keras.optimizers import Adam


from vanillaNN import df_preprocess

EPOCHS = 80
BATCH_SIZE = 128
L_RATE1 = 1e-4
L_RATE2 = 1e-4



class BayesianDenseLayer(tf.keras.layers.Layer):
  def __init__(self, units=32):
    super(BayesianDenseLayer, self).__init__()
    self.units = units

  def build(self, input_shape):
    self.w_mean = tf.Variable(
      tf.keras.initializers.GlorotUniform()(shape=(input_shape[-1], self.units)), 
      name='w_mean'
    )
    self.w_std = tf.Variable(
      tf.keras.initializers.GlorotUniform()(shape=(input_shape[-1], self.units))-10,
      name='w_std'
    )
    self.b_mean = tf.Variable(
      tf.keras.initializers.GlorotUniform()(shape=(self.units,)), 
      name='b_mean'
    )
    self.b_std = tf.Variable(
      tf.keras.initializers.GlorotUniform()(shape=(self.units,))-10, 
      name='b_std'
    )

  def call(self, x, sampling=True):
    """
    Performs the forward pass of the data through the layer.
    """
    if sampling:
      # Flipout-estimated weight samples
      s = tfp.random.rademacher(tf.shape(x))
      r = tfp.random.rademacher([x.shape[0], self.units])
      w_samples = tf.nn.softplus(self.w_std)*tf.random.normal([x.shape[-1], self.units])
      w_perturbations = r*tf.matmul(x*s, w_samples)
      w_outputs = tf.matmul(x, self.w_mean) + w_perturbations
      
      # Flipout-estimated bias samples
      r = tfp.random.rademacher([x.shape[0], self.units])
      b_samples = tf.nn.softplus(self.b_std)*tf.random.normal([self.units])
      b_outputs = self.b_mean + r*b_samples
      
      return w_outputs + b_outputs
      
    else:
      return x @ self.w_mean + self.b_mean

  @property
  def losses(self):
    """
      Sum of the Kullback–Leibler divergences between
      the posterior distributions and their priors.
    """
    weight = tfd.Normal(self.w_mean, tf.nn.softplus(self.w_std))
    bias = tfd.Normal(self.b_mean, tf.nn.softplus(self.b_std))
    prior = tfd.Normal(0, 1)
    return (tf.reduce_sum(tfd.kl_divergence(weight, prior)) +
            tf.reduce_sum(tfd.kl_divergence(bias, prior)))

class BayesianDenseNetwork(tf.keras.Model):
  """ Multilayer fully-connected Bayesian neural network."""

  def __init__(self, dims, name=None):
    """
    Args:
      dims : List[int]
        List of units in each layer
      name : str
        Name for the network
    """
    super(BayesianDenseNetwork, self).__init__(name=name)

    self.steps = []
    self.acts = []
    for units in dims:
      self.steps += [BayesianDenseLayer(units)]
      self.acts += [tf.nn.relu]
      
    self.acts[-1] = lambda x: x

  def call(self, x, sampling=True):
    """Perform the forward pass"""
    for i in range(len(self.steps)):
      x = self.steps[i](x, sampling=sampling)
      x = self.acts[i](x)
    return x

  @property
  def losses(self):
    """
      Sum of the Kullback–Leibler divergences between
      the posterior distributions and their priors.
    """
    return tf.reduce_sum([s.losses for s in self.steps])

class BayesianDenseRegression(tf.keras.Model):
  """ Multilayer Bayesian neural network regressoion."""

  def __init__(self, dims, name=None):
    """
    Args:
      dims : List[int]
        List of units in each layer
      name : str
        Name for the network
    """
    super(BayesianDenseRegression, self).__init__(name=name)
    
    # Multilayer fully-connected neural network to predict mean
    self.mean_net = BayesianDenseNetwork(dims)
    # Variational distribution variables for observation error
    self.std_alpha = tf.Variable([10.0], name='std_alpha')
    self.std_beta = tf.Variable([10.0], name='std_beta')

  def call(self, x, sampling=True):
    """
      Perform the forward pass of the data through
      the network, predicting both means and stds.
    """
    
    # Predict means
    mean_preds = self.mean_net(x, sampling=sampling)

    # Predict std deviation using square-root inverse Gamma distribution
    posterior = tfd.Gamma(self.std_alpha, self.std_beta)
    transform = lambda x: tf.sqrt(tf.math.reciprocal(x))
    N = x.shape[0]
    if sampling:
        std_preds = transform(posterior.sample([N]))
    else:
        std_preds = tf.ones([N, 1])*transform(posterior.mean())
    # Return mean and std predictions
    return tf.concat([mean_preds, std_preds], 1)

  def log_likelihood(self, x, y, sampling=True):
    """
      Compute the log likelihood of some data (y) 
      given current parameters values (x).
    """
    
    # Compute mean and std predictions
    preds = self.call(x, sampling=sampling)
    
    # Return log likelihood of true data given predictions
    return tfd.Normal(preds[:,0], preds[:,1]).log_prob(y[:,0])

  @tf.function
  def sample(self, x):
    """Draw one sample from the predictive distribution."""
    preds = self.call(x)
    return tfd.Normal(preds[:,0], preds[:,1]).sample()

  def samples(self, x, n_samples=1):
    """Draw multiple samples from the predictive distribution."""
    samples = np.zeros((x.shape[0], n_samples))
    for i in range(n_samples):
      samples[:,i] = self.sample(x)
    return samples

  @property
  def losses(self):
    """
      Sum of the Kullback–Leibler divergences between
      the posterior distributions and their priors, 
      over all layers in the network.
    """   
    # Loss due to network weights
    net_loss = self.mean_net.losses

    # Loss due to std deviation parameter
    posterior = tfd.Gamma(self.std_alpha, self.std_beta)
    prior = tfd.Gamma(10.0, 10.0)
    std_loss = tfd.kl_divergence(posterior, prior)

    # Return the sum of both
    return net_loss + std_loss

class BayesianDenseSubNetwork(tf.keras.Model):
  """
  Multilayer fully-connected Bayesian neural network,
  with sub-networks to predict both the mean and the standard deviation.
  """
  
  def __init__(self, units, sub_units, name=None):
    """
    Args:
      units: List[int]
        Number of units per layer in the core network.
      sub_units: List[int]
        Number of units per layer in the sub networks.
      name : None or str
        Name for the layer
    """
    # Initialize
    super(BayesianDenseSubNetwork, self).__init__(name=name)
    
    # Create sub-networks
    self.core_net = BayesianDenseNetwork(units)
    self.mean_net = BayesianDenseNetwork([units[-1]]+sub_units)
    self.std_net = BayesianDenseNetwork([units[-1]]+sub_units)

  def call(self, x, sampling=True):
    """
    Pass data through the model
    
    Args:
      x : tf.Tensor
        Input data
      sampling : bool
        Whether to sample parameter values from their 
        variational distributions (if True, the default), or
        just use the Maximum a Posteriori parameter value
        estimates (False).
      
    Return: tf.Tensor of shape (Nsamples, 2)
      Output of this model, the predictions.  First column is
      the mean predictions, and second column is the standard
      deviation predictions.
    """
    
    # Pass data through core network
    x = self.core_net(x, sampling=sampling)
    x = tf.nn.relu(x)
    
    # Make predictions with each sub network
    mean_preds = self.mean_net(x, sampling=sampling)
    std_preds = self.std_net(x, sampling=sampling)
    std_preds = tf.nn.softplus(std_preds)
    
    # Return mean and std predictions
    return tf.concat([mean_preds, std_preds], 1)

  def log_likelihood(self, x, y, sampling=True):
    """Compute the log likelihood of y given x"""
    
    # Compute mean and std predictions
    preds = self.call(x, sampling=sampling)
    
    # Return log likelihood of true data given predictions
    return tfd.Normal(preds[:,0], preds[:,1]).log_prob(y[:,0])
          
  @tf.function
  def sample(self, x):
    """Draw one sample from the predictive distribution"""
    preds = self.call(x)
    return tfd.Normal(preds[:,0], preds[:,1]).sample()
  
  def samples(self, x, n_samples=1):
    """Draw multiple samples from predictive distributions"""
    samples = np.zeros((x.shape[0], n_samples))
    for i in range(n_samples):
      samples[:,i] = self.sample(x)
    return samples
  
  @property
  def losses(self):
    """Sum of the KL divergences between priors and posteriors"""
    return (
      self.core_net.losses+self.mean_net.losses+self.std_net.losses
    )


if __name__=='__main__':
  # Read the training data
  path_to_dataset = os.path.join(os.path.dirname(__file__), '..', 'data')
  train = pd.read_csv(os.path.join(path_to_dataset,'train_data.csv'))

  df = df_preprocess(train)

  df['risk_class']=np.nan
  df.loc[df['risk']>=-6.0,'risk_class'] = int(1)
  df.loc[df['risk']<-6.0,'risk_class'] = int(0)

  # Encodes the c_object_type column values
  le = preprocessing.LabelEncoder()
  df['c_object_type'] = le.fit_transform(df['c_object_type'])
  
  # Drop columns with missing values
  df.dropna(axis='columns', inplace=True)

  # Split the data for xgboost
  X = df.drop(
    [
      #'event_id',
      'target_risk'
    ], 
    axis=1
  )
  y = df['target_risk'].copy()

  # Train-test split
  X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=42, stratify=(y>=-6).astype(int)
  )
  
  event_id_train = X_train['event_id'].values
  event_id_test = X_test['event_id'].values
  LRP_train = X_train['risk_class'].copy()
  LRP_test = X_test['risk_class'].copy()
  X_train.drop(columns=['risk_class', 'event_id'], inplace=True)
  X_test.drop(columns=['risk_class', 'event_id'], inplace=True)
  
  # Standardization
  sc = preprocessing.StandardScaler()
  X_train = sc.fit_transform(X_train)
  X_test = sc.transform(X_test)

  N_train = X_train.shape[0]
  N_test = X_test.shape[0]

  # Make y 2d
  y_train = np.expand_dims(y_train.values, 1)
  y_test = np.expand_dims(y_test.values, 1)

  # Make a TensorFlow Dataset from training data
  data_train = tf.data.Dataset.from_tensor_slices(
    (X_train.astype('float32'), y_train.astype('float32'))).shuffle(10000).batch(BATCH_SIZE)
  # Make a TensorFlow Dataset from validation data
  data_test = tf.data.Dataset.from_tensor_slices(
    (X_test.astype('float32'), y_test.astype('float32'))).batch(N_test)

  model1 = BayesianDenseRegression([512, 128, 64, 32, 1])
  model2 = BayesianDenseSubNetwork([512, 256, 128, 64], [128, 64, 32, 1])


  # TODO: handle 'ValueError: tf.function-decorated 
  # function tried to create variables on non-first call'.
  
  # Adam optimizer
  optimizer = tf.keras.optimizers.Adam(lr=L_RATE1)
  
  @tf.function
  def train_step(x_data, y_data):
    """Executes one training step and returns the elbo loss.

    This function computes the loss and gradients, and uses the latter to
    update the model's parameters.
    """
    with tf.GradientTape() as tape:
      log_likelihoods = model1.log_likelihood(x_data, y_data)
      kl_loss = model1.losses
      # these two terms need to be on the same scale
      elbo_loss = kl_loss/N_train - tf.reduce_mean(log_likelihoods)
    gradients = tape.gradient(elbo_loss, model1.trainable_variables)
    optimizer.apply_gradients(zip(gradients, model1.trainable_variables))
    return elbo_loss

  # Fit the model
  elbo1 = np.zeros(EPOCHS)
  mae1 = np.zeros(EPOCHS)
  for epoch in range(EPOCHS):
    # Update weights each batch
    for x_data, y_data in data_train:
      elbo1[epoch] += train_step(x_data, y_data)
        
    # Evaluate performance on validation data
    for x_data, y_data in data_test:
      y_pred1 = model1(x_data, sampling=False)[:, 0]
      mae1[epoch] = mean_absolute_error(y_pred1, y_data)
      

  # Adam optimizer
  optimizer = tf.keras.optimizers.Adam(lr=L_RATE2)
  
  @tf.function
  def train_step(x_data, y_data):
    """Executes one training step and returns the elbo loss.

    This function computes the loss and gradients, and uses the latter to
    update the model's parameters.
    """
    with tf.GradientTape() as tape:
      log_likelihoods = model2.log_likelihood(x_data, y_data)
      kl_loss = model2.losses
      # these two terms need to be on the same scale
      elbo_loss = kl_loss/N_train - tf.reduce_mean(log_likelihoods)
    gradients = tape.gradient(elbo_loss, model2.trainable_variables)
    optimizer.apply_gradients(zip(gradients, model2.trainable_variables))
    return elbo_loss

  # Fit the model
  elbo2 = np.zeros(EPOCHS)
  mae2 = np.zeros(EPOCHS)
  for epoch in range(EPOCHS):
    # Update weights each batch
    for x_data, y_data in data_train:
      elbo2[epoch] += train_step(x_data, y_data)
        
    # Evaluate performance on validation data
    for x_data, y_data in data_test:
      y_pred2 = model2(x_data, sampling=False)[:, 0]
      mae2[epoch] = mean_absolute_error(y_pred2, y_data)


  # Plot the ELBO loss
  plt.plot(elbo1, label='Model 1')
  plt.plot(elbo2, label='Model 2')
  plt.xlabel('Epoch')
  plt.ylabel('ELBO Loss')
  plt.legend()
  plt.show()

  # Plot validation error over training
  plt.plot(mae1, label='Model 1')
  plt.plot(mae2, label='Model 2')
  plt.xlabel('Epoch')
  plt.ylabel('Mean Absolute Error')
  plt.legend()
  plt.show()

  y_pred_train_list1 = []
  y_pred_train_list2 = []
  for x_data, y_data in data_train:
    y_pred_train_list1.append(model1(x_data, sampling=False)[:, 0].numpy())
    y_pred_train_list2.append(model2(x_data, sampling=False)[:, 0].numpy())
  y_pred_train1 = np.concatenate(y_pred_train_list1)
  y_pred_train2 = np.concatenate(y_pred_train_list2)

  for x_data, y_data in data_test:
    y_pred_test1 = model1(x_data, sampling=False)[:, 0].numpy()
    y_pred_test2 = model2(x_data, sampling=False)[:, 0].numpy()

  y_pred_train_clf1 = (y_pred_train1>=-6).astype(int)
  y_pred_train_clf2 = (y_pred_train2>=-6).astype(int)
  y_train_clf = (y_train>=-6).astype(int)
  y_pred_test_clf1 = (y_pred_test1>=-6).astype(int)
  y_pred_test_clf2 = (y_pred_test2>=-6).astype(int)
  y_test_clf = (y_test>=-6).astype(int)
  
  print('\n\n------------ Classification ------------')
  print('LRP train performance')
  print(confusion_matrix(y_train_clf, LRP_train))
  print(classification_report(y_train_clf, LRP_train))
  print('BNN train performance:')
  print(confusion_matrix(y_train_clf,y_pred_train_clf1))
  print(classification_report(y_train_clf,y_pred_train_clf1))
  print('BNN (sub nets) train performance:')
  print(confusion_matrix(y_train_clf,y_pred_train_clf2))
  print(classification_report(y_train_clf,y_pred_train_clf2))
  
  print('LRP test performance')
  print(confusion_matrix(y_test_clf, LRP_test))
  print(classification_report(y_test_clf, LRP_test))
  print('BNN test performance:')
  print(confusion_matrix(y_test_clf,y_pred_test_clf1))
  print(classification_report(y_test_clf,y_pred_test_clf1))
  print('BNN (sub nets) test performance:')
  print(confusion_matrix(y_test_clf,y_pred_test_clf2))
  print(classification_report(y_test_clf,y_pred_test_clf2))
  


  # Sample from predictive distributions  
  for x_data, y_data in data_test:
    samples1 = model1.samples(x_data, 500)
    samples2 = model2.samples(x_data, 500)
  # Plot predictive distributions
  plt.figure(figsize=(10, 4))
  high_list = []
  for i, v in enumerate(y_data):
    if v[-1]>=-6:
      high_list.append(i)
  for k, kk in enumerate(high_list):
    plt.subplot(3,5,k+1)
    sns.kdeplot(samples1[kk,:], shade=True)
    sns.kdeplot(samples2[kk,:], shade=True)
    plt.axvline(y_data.numpy()[kk], ls=':', color='grey')
    plt.xlim([-35, 0])
    plt.ylim([0, 1])
    #plt.gca().get_yaxis().set_ticklabels([])
    #plt.gca().get_xaxis().set_ticklabels([])
    plt.title('Event ID %d' % (event_id_test[kk]))
    print(y_data.numpy()[kk])

  plt.show()
  
