import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.stats import norm

from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.metrics import fbeta_score, precision_score
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.metrics import mean_absolute_error, mean_squared_error
from sklearn.utils import shuffle

import tensorflow_probability as tfp
import tensorflow as tf 
tfd = tfp.distributions
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, BatchNormalization
from tensorflow.keras.layers import ReLU, Dropout
from tensorflow.keras.optimizers import Adam

from vanillaNN import df_preprocess
from bayesianNN import BayesianDenseLayer, BayesianDenseNetwork, BayesianDenseRegression

EPOCHS = 5000
BATCH_SIZE = 256
L_RATE = 1e-4
THRESHOLD = -6
CLIP = False

tf.random.set_seed(19)

def plot_confusion_matrix(cm, target_names,title='Confusion matrix',cmap=None,normalize=True):
  """
  given a sklearn confusion matrix (cm), make a nice plot

  Arguments
  ---------
  cm:           confusion matrix from sklearn.metrics.confusion_matrix

  target_names: given classification classes such as [0, 1, 2]
                the class names, for example: ['high', 'medium', 'low']

  title:        the text to display at the top of the matrix

  cmap:         the gradient of the values displayed from matplotlib.pyplot.cm
                see http://matplotlib.org/examples/color/colormaps_reference.html
                plt.get_cmap('jet') or plt.cm.Blues

  normalize:    If False, plot the raw numbers
                If True, plot the proportions

  Usage
  -----
  plot_confusion_matrix(cm           = cm,                  # confusion matrix created by
                                                            # sklearn.metrics.confusion_matrix
                        normalize    = True,                # show proportions
                        target_names = y_labels_vals,       # list of names of the classes
                        title        = best_estimator_name) # title of graph

  Citiation
  ---------
  http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html

  """
  import matplotlib.pyplot as plt
  import numpy as np
  import itertools

  # accuracy = np.trace(cm) / float(np.sum(cm))
  # misclass = 1 - accuracy

  if cmap is None:
    cmap = plt.get_cmap('Blues')

  plt.figure(figsize=(8, 6))
  plt.imshow(cm, interpolation='nearest', cmap=cmap)
  plt.title(title, fontsize=18)
  cb = plt.colorbar()
  cb.ax.tick_params(labelsize=18)

  if target_names is not None:
    tick_marks = np.arange(len(target_names))
    plt.xticks(tick_marks, target_names, size=18)
    plt.yticks(tick_marks, target_names, size=18)

  if normalize:
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]


  thresh = cm.max() / 1.5 if normalize else cm.max() / 2
  for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
    if normalize:
      plt.text(
        j, 
        i, 
        "{:0.4f}".format(cm[i, j]),
        horizontalalignment="center",
        color="white" if cm[i, j] > thresh else "black"
      )
    else:
      plt.text(
        j, 
        i, 
        "{:,}".format(cm[i, j]),
        horizontalalignment="center",
        color="white" if cm[i, j] > thresh else "black",
        size=18
      )
      
  plt.tight_layout()
  plt.ylabel('True label', fontsize=18)
  plt.xlabel('Predicted label', fontsize=18)
  plt.show()

if __name__=='__main__':
  # Read the training data
  path_to_dataset = os.path.join(os.path.dirname(__file__), '..', 'data')
  #train = pd.read_csv(os.path.join(path_to_dataset,'train_data.csv'))
  #test = pd.read_csv(os.path.join(path_to_dataset,'test_data.csv'))
  train = pd.read_csv(os.path.join(path_to_dataset,'new_data_train.csv'))
  test = pd.read_csv(os.path.join(path_to_dataset,'new_data_test.csv'))

  #df = df_preprocess(train)

  # clipping at a risk of -6.001
  if CLIP == True:
    train.loc[train['target_risk']<THRESHOLD,'target_risk']=THRESHOLD-10**(-3)
    train.loc[df['risk']<THRESHOLD,'risk']=THRESHOLD-10**(-3)

  # the model’s target was defined to be the change in
  # risk value between the input CDM and the event’s final CDM. 
  # It allows for a direct comparison to the LRP baseline as 
  # the model predicts a new estimator h such that r = LRP + h.
  train['target'] = train['target_risk'] - train['risk']
  test['target'] = test['target_risk'] - test['risk']

  train['risk_class'] = 0
  train['risk_class'].where(train['target_risk']>=THRESHOLD, 1, inplace=True)
  test['risk_class'] = 0
  test['risk_class'].where(test['target_risk']>=THRESHOLD, 1, inplace=True)

  # Encodes the c_object_type column values
  le = preprocessing.LabelEncoder()
  train['c_object_type'] = le.fit_transform(train['c_object_type'])
  test['c_object_type'] = le.transform(test['c_object_type'])

  # Drop columns with missing values
  train.dropna(axis='columns', inplace=True)
  test.dropna(axis='columns', inplace=True)

  # Data split
  X = train.drop(columns=[
    'mission_id',
    'c_object_type',
    'predicted_risk_anomaly',
    #'risk',
    #'target_risk',
    #'target'
  ])
  y = train['risk_class'].copy()

  X_test = test.drop(columns=[
    'mission_id',
    'c_object_type',
    'predicted_risk_anomaly',
    #'risk',
    #'target_risk',
    #'target'
  ])
  y_test = test['risk_class'].copy()

  # Train-test split
  X_train, X_val, y_train, y_val = train_test_split(
    X, y, test_size=0.2, random_state=42, stratify=y
  )
  
  target_risk_train = X_train['target_risk']
  target_risk_val = X_val['target_risk']
  target_risk_X = X['target_risk']
  target_risk_test = X_test['target_risk']
  risk_train = X_train['risk']
  risk_val = X_val['risk']
  risk_X = X['risk']
  risk_test = X_test['risk']
  y_train = X_train['target']
  y_val = X_val['target']
  y = X['target']
  y_test = X_test['target']
  event_id_train = X_train['event_id'].values
  event_id_val = X_val['event_id'].values
  event_id_X = X['event_id'].values
  event_id_test = X_test['event_id'].values
  X_train.drop(columns=['risk','target','target_risk','event_id'], inplace=True)
  X_val.drop(columns=['risk','target','target_risk','event_id'], inplace=True)
  X.drop(columns=['risk','target','target_risk','event_id'], inplace=True)
  X_test.drop(columns=['risk','target','target_risk','event_id'], inplace=True)
  
  
  # Standardization
  sc1 = preprocessing.StandardScaler()
  X_train = sc1.fit_transform(X_train)
  X_val = sc1.transform(X_val)

  sc2 = preprocessing.StandardScaler()
  X = sc2.fit_transform(X)
  X_test = sc2.transform(X_test)
  

  model = BayesianDenseRegression([512, 128, 64, 32, 1], seed=12)
  # Adam optimizer
  optimizer = tf.keras.optimizers.Adam(lr=L_RATE)

  def train_model(X_train, y_train, X_test, y_test, epochs=EPOCHS):

    N_train = X_train.shape[0]
    N_test = X_test.shape[0]

    # Make y 2d
    y_train = np.expand_dims(y_train.values, 1)
    y_test = np.expand_dims(y_test.values, 1)

    # Make a TensorFlow Dataset from training data
    data_train = tf.data.Dataset.from_tensor_slices(
      (X_train.astype('float32'), y_train.astype('float32'))
    ).shuffle(10000).batch(BATCH_SIZE)
  
    # Make a TensorFlow Dataset from test data
    data_test = tf.data.Dataset.from_tensor_slices(
      (X_test.astype('float32'), y_test.astype('float32'))
    ).batch(N_test)

  
    @tf.function
    def train_step(x_data, y_data):
      """Executes one training step and returns the elbo loss.

      This function computes the loss and gradients, and uses the latter to
      update the model's parameters.
      """
      with tf.GradientTape() as tape:
        log_likelihoods = model.log_likelihood(x_data, y_data)
        kl_loss = model.losses
        # these two terms need to be on the same scale
        elbo_loss = kl_loss/N_train - tf.reduce_mean(log_likelihoods)
      gradients = tape.gradient(elbo_loss, model.trainable_variables)
      optimizer.apply_gradients(zip(gradients, model.trainable_variables))
      return elbo_loss

    # Fit the model
    elbo_train = np.zeros(epochs)
    elbo_test = np.zeros(epochs)
    mae = np.zeros(epochs)
    for epoch in range(epochs):
      # Update weights each batch
      for x_data, y_data in data_train:
        elbo_train[epoch] += train_step(x_data, y_data)
      # Evaluate performance on validation data
      for x_data, y_data in data_test:
        y_pred = model(x_data, sampling=False)[:, 0]
        log_likelihoods = model.log_likelihood(x_data, y_data)
        kl_loss = model.losses
        # these two terms need to be on the same scale
        elbo_test[epoch] += kl_loss/N_test - tf.reduce_mean(log_likelihoods)
        mae[epoch] = mean_absolute_error(y_pred, y_data)
      


    # Plot the ELBO loss
    plt.plot(elbo_train, label='Train')
    plt.plot(elbo_test, label='Val.')
    plt.xlabel('Epoch')
    plt.ylabel('ELBO Loss')
    plt.legend()
    plt.show()

    # Plot validation error over training
    plt.plot(mae)
    plt.xlabel('Epoch')
    plt.ylabel('Mean Absolute Error')
    plt.show()

    return np.argmin(elbo_test), data_train, data_test
  

  sweet_spot, _, _ = train_model(X_train, y_train, X_val, y_val, epochs=EPOCHS)
  print(sweet_spot)
  model = BayesianDenseRegression([512, 128, 64, 32, 1], seed=12)
  # Adam optimizer
  optimizer = tf.keras.optimizers.Adam(lr=L_RATE)
  _, data_train, data_test = train_model(X, y, X_test, y_test, epochs=sweet_spot)

  y_pred_X_list = []
  std_pred_X_list = []
  for x_data, y_data in data_train:
    y_pred_X_list.append(model(x_data, sampling=False)[:, 0].numpy())
    std_pred_X_list.append(model(x_data, sampling=False)[:, 1].numpy())
  y_pred_X = np.concatenate(y_pred_X_list)
  std_pred_X = np.concatenate(std_pred_X_list)

  
  for x_data, y_data in data_test:
    y_pred_test = model(x_data, sampling=False)[:, 0].numpy()
    std_pred_test = model(x_data, sampling=False)[:, 1].numpy()

  y_pred_X_clf = ((y_pred_X+risk_X.values)>=-6).astype(int)
  y_X_clf = ((y.ravel()+risk_X.values)>=-6).astype(int)
  y_pred_test_clf = ((y_pred_test+risk_test.values)>=-6).astype(int)
  y_test_clf = ((y_test.ravel()+risk_test.values)>=-6).astype(int)
  
  LRP_X = np.where(risk_X.values>=THRESHOLD, 1, 0)
  LRP_test = np.where(risk_test.values>=THRESHOLD, 1, 0)
  # LRP_train = np.where(risk_train.values>=THRESHOLD, risk_train.values, THRESHOLD-10**(-3))
  # LRP_test = np.where(risk_test.values>=THRESHOLD, risk_test.values, THRESHOLD-10**(-3))


  print('\n\n------------ Regression ------------')
  if CLIP == False:
    #print('MAE: %.2f' % (mean_absolute_error(y_val, y_pred_val)))
    print('MSE: %.2f' % (mean_squared_error(y_test, y_pred_test)))
  if CLIP == True:
    err = np.where(
      y_test_clf==1, 
      np.abs(y_pred_test-y_test), 
      np.maximum(0, y_pred_test-THRESHOLD)
    )
    print('MAE: %.2f' % np.mean(err))


  print('\n\n------------ Classification ------------')

  print('LRP train performance')
  print(confusion_matrix(y_X_clf, LRP_X))
  print(classification_report(y_X_clf, LRP_X))
  print('BNN train performance:')
  print(confusion_matrix(y_X_clf,y_pred_X_clf))
  print(classification_report(y_X_clf,y_pred_X_clf))
  

  print('LRP test performance')
  print(confusion_matrix(y_test_clf, LRP_test))
  print(classification_report(y_test_clf, LRP_test))
  print('BNN test performance:')
  print(confusion_matrix(y_test_clf,y_pred_test_clf))
  print(classification_report(y_test_clf,y_pred_test_clf))
  
  cm_baseline = confusion_matrix(y_test_clf, LRP_test)
  plot_confusion_matrix(
    cm_baseline, 
    normalize=False, 
    target_names=[0,1],
    title='Baseline Confusion Matrix'
  )
  cm_model = confusion_matrix(y_test_clf,y_pred_test_clf)
  plot_confusion_matrix(
    cm_model, 
    normalize=False, 
    target_names=[0,1],
    title='Bayesian Neural Network Confusion Matrix'
  )
  

  # Sample from predictive distributions  
  for x_data, y_data in data_test:
    samples = model.samples(x_data, 1000)

  # Plot predictive distributions
  plt.figure(figsize=(10, 8))
  high_list = []
  for i, v in enumerate(zip(y_data, risk_test.values)):
    if sum(v)>=-6:
      high_list.append(i)

  def covered(samples, y_true, prc=95.0):
    """Whether each sample was covered by predictive interval"""
    q0 = (100.0-prc)/2.0 #lower percentile 
    q1 = 100.0-q0        #upper percentile
    within_conf_int = np.zeros(len(y_true))
    for i in range(len(y_true)):
      p0 = np.percentile(samples[i], q0)
      p1 = np.percentile(samples[i], q1)
      if p0<=y_true[i] and p1>y_true[i]:
        within_conf_int[i] = 1
    return within_conf_int
  samples_high = []
  y_true_high = []
  event_id_list = []
  baseline_list = []
  for k, kk in enumerate(high_list):
    plt.subplot(6,7,k+1)
    sns.kdeplot(samples[kk,:]+risk_test.values[kk], shade=True)
    plt.axvline(
      y_data.numpy()[kk]+risk_test.values[kk], 
      ls=':', 
      color='grey', 
      label='y_true'
    )
    plt.axvline(
      -6, 
      ls=':', 
      color='red',
      label='-6'
    )
    plt.axvline(
      risk_test.values[kk], 
      ls=':', 
      color='green',
      label='baseline'
    )
    plt.xlim([-15, 0])
    plt.ylim([0, 1])
    #plt.gca().get_yaxis().set_ticklabels([])
    #plt.gca().get_xaxis().set_ticklabels([])
    plt.legend()
    plt.title('Event ID %d' % (event_id_test[kk]))
    #print(y_data.numpy()[kk]+risk_test.values[kk]) 
    #print(risk_test.values[kk])
    #print(y_pred_test[kk]+risk_test.values[kk])
    # print(
    #   'Event ID %d -- probability of y_pred>y_true: %3.2f%%' % (
    #     event_id_test[kk],
    #     (1-norm.cdf(
    #       y_data.numpy()[kk]+risk_test.values[kk],
    #       y_pred_test[kk]+risk_test.values[kk],
    #       std_pred_test[kk]
    #     ))*100
    #   )
    # )   
    # print(
    #   'Event ID %d -- probability of y_pred>baseline: %3.2f%%' % (
    #     event_id_test[kk],
    #     (1-norm.cdf(
    #       risk_test.values[kk],
    #       y_pred_test[kk]+risk_test.values[kk],
    #       std_pred_test[kk]
    #     ))*100
    #   )
    # ) 
    event_id_list.append(event_id_test[kk])
    samples_high.append(samples[kk,:]+risk_test.values[kk])
    y_true_high.append(y_data.numpy()[kk]+risk_test.values[kk])
    baseline_list.append(risk_test.values[kk])

  print(
    'Samples covered by 25% predictive intervals: ', 
    100*np.mean(covered(samples_high, y_true_high, 25))
  )
  print(
    'Samples covered by 50% predictive intervals: ', 
    100*np.mean(covered(samples_high, y_true_high, 50))
  )
  print(
    'Samples covered by 75% predictive intervals: ', 
    100*np.mean(covered(samples_high, y_true_high, 75))
  )
  print(
    'Samples covered by 95% predictive intervals: ', 
    100*np.mean(covered(samples_high, y_true_high))
  )
  
  plt.show()

  fig, ax = plt.subplots(figsize=(30, 8))
  ax.violinplot(samples_high)
  ax.axhline(-6, linestyle='--', color='grey')
  ax.scatter(
    np.arange(1, len(event_id_list) + 1), 
    y_true_high, 
    color='navy',
    label='y_true'
  )
  ax.scatter(
    np.arange(1, len(event_id_list) + 1), 
    baseline_list, 
    color='teal',
    label='Baseline'
  )
  ax.get_xaxis().set_tick_params(direction='out', rotation=50)
  ax.xaxis.set_ticks_position('bottom')
  ax.set_xticks(np.arange(1, len(event_id_list) + 1))
  ax.set_xticklabels(event_id_list)
  ax.tick_params(axis='both', labelsize=18)
  ax.set_xlim(0.25, len(event_id_list) + 0.75)
  ax.set_xlabel('Event ID', fontsize=18)
  ax.set_ylabel('Risk', fontsize=18)
  plt.legend(fontsize=14, loc='best')
  plt.grid(True, linestyle=':')
  plt.show()
