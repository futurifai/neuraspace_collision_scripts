import os
import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt

from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, classification_report

import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, BatchNormalization
from tensorflow.keras.layers import ReLU, Dropout
from tensorflow.keras.optimizers import Adam


BATCH_SIZE = 512
EPOCHS = 1000
L_RATE = 1e-4
VAL_SPLIT = 0.2

def df_preprocess(df):
  """Prepare dataset with data that uses last available CDM.
  Args: 
    df: DataFrame to prepocess.
  Returns:
    Processed DataFrame.
  """

  # Number of events that has time_to_tca larger and smaller 
  # than 2 at the same time
  events_less_than_two_days = df[
    df["time_to_tca"]<1.0
  ].groupby("event_id")["event_id"].first().tolist()
  events_more_than_two_days = df[
    df["time_to_tca"]>2.0
  ].groupby("event_id")["event_id"].first().tolist()
  events_trainable = [value for value in events_less_than_two_days \
    if value in events_more_than_two_days]


  # Generates new training data that uses last available CDM 
  # (until 2 days to tca) and adds latest CDM risk as target variable
  lenData = len(events_trainable)
  new_df = []
  target_variable = []
  no_larger_2 = []
  mean_larger_2 =[]
  std_larger_2 = []
  mean_nan_number =[]
  std_nan_number = []
  for cnt in range(lenData):
    new_df.append(
      df[
        (df["event_id"]==events_trainable[cnt])&(df["time_to_tca"]>=2.0)
      ][-1:].values.tolist()[0]
    )
    target_variable.append(
      df[(df["event_id"]==events_trainable[cnt])][-1:].risk.values[0]
    )
    no_larger_2.append(
      df[
        (df["event_id"]==events_trainable[cnt])&(df["time_to_tca"]>=2.0)
      ].shape[0]
    )
    mean_larger_2.append(
      df[
        (df["event_id"]==events_trainable[cnt])&(df["time_to_tca"]>=2.0)
      ].risk.mean()
    )
    std_larger_2.append(
      df[
        (df["event_id"]==events_trainable[cnt])&(df["time_to_tca"]>=2.0)
      ].risk.std(ddof=0)
    )
    mean_nan_number.append(
      df[
        (df["event_id"]==events_trainable[cnt])&(df["time_to_tca"]>=2.0)
      ].isnull().sum(axis=1).mean()
    )
    std_nan_number.append(
      df[
        (df["event_id"]==events_trainable[cnt])&(df["time_to_tca"]>=2.0)
      ].isnull().sum(axis=1).std(ddof=0)
    )
  final_df = pd.DataFrame(new_df,columns=df.columns.tolist())

  # Adds target value to the dataset
  target_variable_pd = pd.DataFrame(target_variable,columns=["target_risk"])
  no_larger_2_pd = pd.DataFrame(no_larger_2,columns=["no_larger_2"])
  mean_larger_2_pd = pd.DataFrame(mean_larger_2,columns=["mean_larger_2"])
  std_larger_2_pd = pd.DataFrame(std_larger_2,columns=["std_larger_2"])
  mean_nan_number_pd = pd.DataFrame(mean_nan_number,columns=["mean_nan_number"])
  std_nan_number_pd = pd.DataFrame(std_nan_number,columns=["std_nan_number"])
  final_df["target_risk"] = target_variable_pd.values
  final_df["no_larger_2"] = no_larger_2_pd.values
  final_df["mean_larger_2"] = mean_larger_2_pd.values
  final_df["std_larger_2"] = std_larger_2_pd.values
  final_df["mean_nan_number"] = mean_nan_number_pd.values
  final_df["std_nan_number"] = std_nan_number_pd.values

  return final_df



if __name__=='__main__':
  # Read the training data
  path_to_dataset = os.path.join(os.path.dirname(__file__), '..', 'data')
  train = pd.read_csv(os.path.join(path_to_dataset,'train_data.csv'))

  df = df_preprocess(train)

  df['risk_class']=np.nan
  df.loc[df['risk']>=-6.0,'risk_class'] = int(1)
  df.loc[df['risk']<-6.0,'risk_class'] = int(0)

  # Encodes the c_object_type column values
  le = preprocessing.LabelEncoder()
  df['c_object_type'] = le.fit_transform(df['c_object_type'])

  # Drop columns with missing values
  df.dropna(axis='columns', inplace=True)

  # Split the data for xgboost
  X = df.drop(
    [
      'event_id',
      'target_risk'
    ], 
    axis=1
  )
  y = df['target_risk'].copy()

  # Train-test split
  X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=VAL_SPLIT, random_state=42, 
    stratify=(y>=-6).astype(int)
  )
  
  LRP_train = X_train['risk_class'].copy()
  LRP_test = X_test['risk_class'].copy()
  X_train.drop(columns='risk_class', inplace=True)
  X_test.drop(columns='risk_class', inplace=True)

  # Standardization
  sc = preprocessing.StandardScaler()
  X_train = sc.fit_transform(X_train)
  X_test = sc.transform(X_test)

  # Multilayer dense neural network using Keras’s Sequential model
  D = X_train.shape[1]
  model = Sequential([
    Dense(512, use_bias=True, input_shape=(D,)),
    BatchNormalization(),
    ReLU(),
    Dropout(0.1),
    Dense(128, use_bias=True),
    BatchNormalization(),
    ReLU(),
    Dropout(0.1),
    Dense(64, use_bias=True),
    BatchNormalization(),
    ReLU(),
    Dropout(0.1),
    Dense(32, use_bias=True),
    BatchNormalization(),
    ReLU(),
    Dropout(0.1),
    Dense(1)
  ])

  # Compile the model with MAE loss
  model.compile(
    tf.keras.optimizers.Adam(lr=L_RATE),
    loss='mean_absolute_error'
  )

  # Fit the model
  history = model.fit(
    X_train, 
    y_train.values,
    epochs=EPOCHS,
    batch_size=BATCH_SIZE,
    validation_data=(X_test, y_test.values),
    verbose=0,
  )


  y_pred_train = model.predict(X_train)
  y_pred_test = model.predict(X_test)
 
  y_pred_train_clf = (y_pred_train>=-6).astype(int)
  y_train_clf = (y_train>=-6).astype(int)
  y_pred_test_clf = (y_pred_test>=-6).astype(int)
  y_test_clf = (y_test>=-6).astype(int)
  
  print('\n\n------------ Classification ------------')
  print('LRP train performance')
  print(confusion_matrix(y_train_clf, LRP_train))
  print(classification_report(y_train_clf, LRP_train))
  print('Vanilla train performance:')
  print(confusion_matrix(y_train_clf,y_pred_train_clf))
  print(classification_report(y_train_clf,y_pred_train_clf))
  
  print('LRP test performance')
  print(confusion_matrix(y_test_clf, LRP_test))
  print(classification_report(y_test_clf, LRP_test))
  print('Vanilla test performance:')
  print(confusion_matrix(y_test_clf,y_pred_test_clf))
  print(classification_report(y_test_clf,y_pred_test_clf))
  

  plt.plot(history.history['loss'], label='Train')
  plt.plot(history.history['val_loss'], label='Val')
  plt.legend()
  plt.xlabel('Epoch')
  plt.ylabel('MAE')
  plt.show()