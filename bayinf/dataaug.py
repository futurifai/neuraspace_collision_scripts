# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pandas as pd
import os
import numpy as np
from bayinf import utlils as ut
from matplotlib import pyplot as plt
import pymc3 as pm

os.chdir('c://Users//Francisco Caldas//Documents//work//neuraspace_collision_scripts') 
#change for your work dir
path_to_dataset = './data/'
# Read the training data
train = pd.read_csv(os.path.join(path_to_dataset,'train_data.csv'))
test = pd.read_csv(os.path.join(path_to_dataset,'test_data.csv'))


train=ut.rep_neg_val_0(train)
events_less_than_two_days = train[train["time_to_tca"]<1.0].groupby("event_id")["event_id"].first().tolist()
events_more_than_two_days = train[train["time_to_tca"]>2.0].groupby("event_id")["event_id"].first().tolist()

events_trainable = [value for value in events_less_than_two_days if value in events_more_than_two_days]
train.describe()




trainr=ut.train_target_creation(train,events_trainable)
anomaly_id=trainr[trainr['predicted_risk_anomaly']==1].index.tolist()

'''
creating the bayesian inference with basic priors
'''

sales = train[train["event_id"]==78]['t_cn_r']
salesx= train[train['event_id']==78]['time_to_tca']
plt.plot(salesx, sales, color='tab:blue')
plt.xlim(max(salesx)+0.05, 0) 
plt.show()

from scipy import interpolate

x = np.arange(0, 2*np.pi+np.pi/4, 2*np.pi/8)
y = np.sin(x)
tck = interpolate.splrep(x, y, s=0)
xnew = np.arange(0, 2*np.pi, np.pi/50)
ynew = interpolate.splev(xnew, tck, der=0)
polyt=np.polynomial.polynomial.polyfit(salesx,sales,2)
plt.plot(salesx,polyt[0]+np.power(salesx,1)*polyt[1] + np.power(salesx,2)*polyt[2],salesx,sales)
polyt=np.float32(polyt)
X= train[train['event_id']==78]['time_to_tca']
X= np.float32(X)
Y = train[train["event_id"]==78]['t_cn_r']
Y = np.float32(Y)
with pm.Model() as model:
    std = pm.Uniform("std", 0, 100)
    
    beta = pm.Normal("beta", mu=polyt[0], sd=10)
    alpha = pm.Normal("alpha", mu=polyt[1], sd=10)
    theta = pm.Normal("theta", mu=polyt[2], sd=10)
    
    mean = pm.Deterministic("mean", alpha + beta * X + theta * np.power(X,2))
    
    obs = pm.Normal("obs", mu=mean, sd=std, observed=Y)
    
    trace = pm.sample(100000, step=None,cores=6)
    burned_trace = trace[20000:]

pm.plots.traceplot(burned_trace, var_names=["std", "beta", "alpha","theta"])

pm.plot_posterior(burned_trace, varnames=["std", "beta", "alpha","theta"])

std_trace = burned_trace['std']
beta_trace = burned_trace['beta']
alpha_trace = burned_trace['alpha']
theta_trace = burned_trace['theta']

std_mean = std_trace.mean()
beta_mean = beta_trace.mean()
alpha_mean = alpha_trace.mean()
theta_mean = theta_trace.mean()


x1 = salesx
mean_trace = alpha_mean + beta_mean * x1 + theta_mean * np.power(x1,2) 
normal_dist = pm.Normal.dist(0, sd=std_mean)
errors = normal_dist.random(size=x1.size)
Y_gen = mean_trace + errors
Y_reg1 = mean_trace

plt.plot(salesx, sales, salesx,Y_gen)

def bayes_data_aug(event,data,nsamples=100000,plwatch=0):
    db=data[data['event_id']==event]
    db1=db.drop(['c_object_type','event_id','time_to_tca','mission_id'],1)
    X= db['time_to_tca']
    um=pd.DataFrame()
    for it in range(0,len(train.columns)):
        Y=db1.iloc[:,it]
        polyt=np.polynomial.polynomial.polyfit(X,Y,2)
        with pm.Model() as model:
            std = pm.Uniform("std", 0, 100)
    
            beta = pm.Normal("beta", mu=polyt[0], sd=10)
            alpha = pm.Normal("alpha", mu=polyt[1], sd=10)
            theta = pm.Normal("theta", mu=polyt[2], sd=10)
    
            mean = pm.Deterministic("mean", alpha + beta * X + theta * np.power(X,2))
    
            obs = pm.Normal("obs", mu=mean, sd=std, observed=Y)    
            trace = pm.sample(nsamples, step=None,cores=6)
       #     burned_trace = trace[int(nsamples*burnin):]
            burned_trace=pm.sample_posterior_predictive(trace,model=model,var_names=["std", "beta", "alpha","theta"])

            
        std_mean = burned_trace['std'].mean()
        beta_mean = burned_trace['beta'].mean()
        alpha_mean = burned_trace['alpha'].mean()
        theta_mean = burned_trace['theta'].mean()
        mean_trace = alpha_mean + beta_mean * X + theta_mean * np.power(X,2)
        normal_dist = pm.Normal.dist(0, sd=std_mean)
        errors = normal_dist.random(size=X.size)
        Y_gen = mean_trace + errors
        
        if plwatch==1 :
            pm.plot_posterior(burned_trace, varnames=["std", "beta", "alpha","theta"])
            plt.plot(salesx, sales, salesx,Y_gen)
            

        um.insert(it,db1[it],Y_gen,True)
    
    um.insert(0, 'event_id', db['event_id']+0.1)
    um.insert(1, 'time_to_tca', db['time_to_tca'])
    um.insert(2, 'mission_id', db['mission_id'])
    um.insert(44, 'c_object_type', db['c_object_type'])
        
        
    return(um)    

syn1=bayes_data_aug(324, train)
        
        
    
    
    
    
    
    
    
##### finding the prior TODO
'''
because there is a very small amount of data, the prior gains a bigger importance,
it is also possible to do bayesian inference with weekly or non-informatuve priors.
'''
dist_names = ['weibull_min','norm','weibull_max','beta',
              'invgauss','uniform','gamma','expon',   
              'lognorm','pearson3','trian']

