# -*- coding: utf-8 -*-
"""
Created on Wed Oct  7 15:07:52 2020

@author: Francisco Caldas
"""
import pandas as pd
import numpy as np
def rep_neg_val_0(db):
    db.loc[db["time_to_tca"]<0, ['time_to_tca']] = 0
    return db

def train_target_creation(train,events_trainable):
    lenData = len(events_trainable)
    new_train = []
    target_variable = []
    no_larger_2 = []
    mean_larger_2 =[]
    std_larger_2 = []
    mean_nan_number =[]
    std_nan_number = []
    for cnt in range(lenData):
        new_train.append(train[(train["event_id"]==events_trainable[cnt])&(train["time_to_tca"]>=2.0)][-1:].values.tolist()[0])
        target_variable.append(train[(train["event_id"]==events_trainable[cnt])][-1:].risk.values[0])
        no_larger_2.append(train[(train["event_id"]==events_trainable[cnt])&(train["time_to_tca"]>=2.0)].shape[0])
        mean_larger_2.append(train[(train["event_id"]==events_trainable[cnt])&(train["time_to_tca"]>=2.0)].risk.mean())
        std_larger_2.append(train[(train["event_id"]==events_trainable[cnt])&(train["time_to_tca"]>=2.0)].risk.std(ddof=0))
        mean_nan_number.append(train[(train["event_id"]==events_trainable[cnt])&(train["time_to_tca"]>=2.0)].isnull().sum(axis=1).mean())
        std_nan_number.append(train[(train["event_id"]==events_trainable[cnt])&(train["time_to_tca"]>=2.0)].isnull().sum(axis=1).std(ddof=0))
    new_traintrain_pd = pd.DataFrame(new_train,columns=train.columns.tolist())
    # Adds target value to the dataset
    target_variable_pd = pd.DataFrame(target_variable,columns=["target_risk"])
    no_larger_2_pd = pd.DataFrame(no_larger_2,columns=["no_larger_2"])
    mean_larger_2_pd = pd.DataFrame(mean_larger_2,columns=["mean_larger_2"])
    std_larger_2_pd = pd.DataFrame(std_larger_2,columns=["std_larger_2"])
    mean_nan_number_pd = pd.DataFrame(mean_nan_number,columns=["mean_nan_number"])
    std_nan_number_pd = pd.DataFrame(std_nan_number,columns=["std_nan_number"])
    new_traintrain_pd["target_risk"] = target_variable_pd.values
    new_traintrain_pd["no_larger_2"] = no_larger_2_pd.values
    new_traintrain_pd["mean_larger_2"] = mean_larger_2_pd.values
    new_traintrain_pd["std_larger_2"] = std_larger_2_pd.values
    new_traintrain_pd["mean_nan_number"] = mean_nan_number_pd.values
    new_traintrain_pd["std_nan_number"] = std_nan_number_pd.values
    
    new_traintrain_pd['target_risk_class']=np.nan
    new_traintrain_pd.loc[new_traintrain_pd['target_risk']>=-6.0,'target_risk_class'] = int(1)
    new_traintrain_pd.loc[new_traintrain_pd['target_risk']<-6.0,'target_risk_class'] = int(0)
    
    new_traintrain_pd['predicted_risk'] = 0 
    new_traintrain_pd.loc[new_traintrain_pd['risk']<-6.0,'predicted_risk'] =  -6.000000000000001
    new_traintrain_pd.loc[new_traintrain_pd['risk']>=-6.0,'predicted_risk'] =  new_traintrain_pd.loc[new_traintrain_pd['risk'] >=-6.0].risk.values
    
    new_traintrain_pd['predicted_risk_anomaly']=np.nan
    new_traintrain_pd.loc[(new_traintrain_pd['predicted_risk']>=-6.0)&(new_traintrain_pd['target_risk']>=-6.0),'predicted_risk_anomaly'] = int(0)
    new_traintrain_pd.loc[(new_traintrain_pd['predicted_risk']<-6.0)&(new_traintrain_pd['target_risk']<-6.0),'predicted_risk_anomaly'] = int(0)
    new_traintrain_pd.loc[(new_traintrain_pd['predicted_risk']>=-6.0)&(new_traintrain_pd['target_risk']<-6.0),'predicted_risk_anomaly'] = int(1)
    new_traintrain_pd.loc[(new_traintrain_pd['predicted_risk']<-6.0)&(new_traintrain_pd['target_risk']>=-6.0),'predicted_risk_anomaly'] = int(1)
    return(new_traintrain_pd)
