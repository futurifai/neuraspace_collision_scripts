import pandas as pd
import os
import numpy as np 
from sklearn import preprocessing
from sklearn.model_selection import KFold, StratifiedKFold 
import lightgbm
from sklearn.metrics import confusion_matrix, fbeta_score, precision_score
from sklearn.metrics import classification_report
from matplotlib import pyplot as plt
from sklearn.utils import shuffle

THRESHOLD = -6

def df_preprocess(df):
  """Prepare dataset with data that uses last available CDM.
  Args: 
    df: DataFrame to prepocess.
  Returns:
    Processed DataFrame.
  """
  # Number of events that has time_to_tca larger and smaller than 2 at the same time
  events_less_than_two_days = df[df["time_to_tca"]<1.0].groupby("event_id")["event_id"].first().tolist()
  events_more_than_two_days = df[df["time_to_tca"]>2.0].groupby("event_id")["event_id"].first().tolist()
  events_trainable = [value for value in events_less_than_two_days if value in events_more_than_two_days]

  # Generates new training data that uses last available CDM (until 2 days to tca) and \
  # adds latest CDM risk as target variable
  lenData = len(events_trainable)
  new_df = []
  target_variable = []
  for cnt in range(lenData):
    new_df.append(df[(df["event_id"]==events_trainable[cnt])&(df["time_to_tca"]>=2.0)][-1:].values.tolist()[0])
    target_variable.append(df[(df["event_id"]==events_trainable[cnt])][-1:].risk.values[0])
  finaldf = pd.DataFrame(new_df,columns=df.columns.tolist())
  # Adds target value to the dataset
  target_variable_pd = pd.DataFrame(target_variable,columns=["target_risk"])
  finaldf["target_risk"] = target_variable_pd.values
  return finaldf


# Metrics
def y_clf(y):
  """Bins the data into two classes. 
  Args:
    y: input array. 
  Returns:
    Output array. 
  """
  yclf = (y>=THRESHOLD).astype(int)
  return yclf
                        
def msehr(y_true, y_pred):
  """Mean squared error (high risk).
  Args:
    y_true: true labels.
    y_pred: predicted labels.
  Returns:
    Metric result.
  """
  ytrue_clf = y_clf(y_true)
  n_star = np.sum(ytrue_clf)
  mse = (1/n_star)*np.sum(ytrue_clf*(np.power(y_true-y_pred,2)))
  return mse

def f2(y_true, y_pred):
  """F2 score.
  Args:
    y_true: true labels.
    y_pred: predicted labels.
  Returns:
    Metric result.
  """
  ytrue_clf = y_clf(y_true)
  ypred_clf = y_clf(y_pred)
  return fbeta_score(ytrue_clf, ypred_clf, beta=2)

def L(y_true, y_pred):
  """ Spacecraft Collision Avoidance Challenge metric. 
  Args:
    y_true: true labels.
    y_pred: predicted labels.
  Returns:
    Metric result.
  """
  f_2 = f2(y_true, y_pred)
  mse = msehr(y_true, y_pred)
  return 1/f_2*mse

# Model 
class Model(object):
  """Regression-Classification model."""

  def __init__(self, classifier, regressor, threshold=THRESHOLD):
    """
    Args:
      classifier: Classifier model.
      regressor: Regression model.
      threshold: threshold for data binning.
    """
    self.threshold = threshold
    self.classifier = classifier
    self.regressor = regressor
  
  def fit(self, X, y):
    """Build model from the training set (X,y).
    Args:
      X: array-like training input.
      y: array-like training target.
    """
    y_clf = (y>=self.threshold)
    self.classifier.fit(X, y_clf)
    
    X_reg, y_reg = X[y_clf], y[y_clf]
    self.regressor.fit(X_reg, y_reg)

  def predict(self, X):
    """Predict class and regression value for X.
    Args:
      X: array-like training input.
    Returns:
      The predicted classes and the predict values.
    """
    y_clf = self.classifier.predict(X)
    y_reg = self.regressor.predict(X)
  
    return np.where(y_clf==0, self.threshold - 10**(-3), y_reg), y_clf 
  

if __name__=='__main__':
  # Read the training data
  path_to_dataset = os.path.join(os.path.dirname(__file__), '..', 'data')
  train = pd.read_csv(os.path.join(path_to_dataset,'train_data.csv'))
  #test = pd.read_csv(os.path.join(path_to_dataset,'test_data.csv'))

  df = df_preprocess(train)

  df['risk_class'] = 0
  df['risk_class'].where(df['target_risk']>=THRESHOLD, 1, inplace=True)


  # Data split
  X = df.drop(columns=[
    'mission_id',
    'c_object_type',
    #'risk',
    #'target_risk',
    'risk_class'
  ])
  y = df['risk_class'].copy()

  fig, axs = plt.subplots(ncols=3, figsize=(15,5))
  cmap = plt.get_cmap("tab10")

  kf = StratifiedKFold(n_splits=3, random_state=1)
  for kk , (train_index, test_index) in enumerate(kf.split(X, y)):
    X_train, X_test = X.iloc[list(train_index)], X.iloc[list(test_index)]
    # y_train, y_test = y.iloc[list(train_index)], y.iloc[list(test_index)]
    # X_train, y_test = shuffle(X_train, y_train)
    X_train = shuffle(X_train)
  
    risk_train = X_train['risk'].copy()
    risk_test = X_test['risk'].copy()
    y_train = X_train['target_risk'].copy()
    y_test = X_test['target_risk'].copy()
    X_train.drop(columns=['target_risk', 'risk'], inplace=True)
    X_test.drop(columns=['target_risk', 'risk'], inplace=True)
      
    #LGBMClassifier
    classifier = lightgbm.LGBMClassifier(
      objective = 'binary',
      learning_rate=0.05,
      n_estimators=25, 
      class_weight='balanced'
    )
    #LGBMRegressor
    regressor = lightgbm.LGBMRegressor(
      learning_rate=0.05,
      n_estimators=25,
      feature_fraction=0.25
    )
    model = Model(classifier, regressor, threshold=THRESHOLD)

    model.fit(X_train.values, y_train.ravel())
    y_pred_train, y_pred_trainclf = model.predict(X_train.values)
    y_pred_test, y_pred_testclf = model.predict(X_test.values)
      
       
    print('------------ %2d ------------' % (kk+1))
    #plt.plot(y_test.ravel()+risk_test.values, y_pred.ravel()+risk_test.values, '.')
    #plt.plot(y_test.ravel(), y_pred.ravel(), '.')
       
    # Baseline
    LRP_train = np.where(risk_train.values>=THRESHOLD, risk_train.values, THRESHOLD-10**(-3))
    LRP_test = np.where(risk_test.values>=THRESHOLD, risk_test.values, THRESHOLD-10**(-3))
    # plt.plot(LRP, risk_train.values, '.')
    # plt.plot(LRP, y_clf(LRP), '.')
    # plt.plot(y_clf(y_true_trainclf), y_clf(LRP), '.')
    # plt.plot(y_true_trainclf, y_clf(y_true_trainclf), '.')
    # plt.show()
    
    # 1/F2 Score
    f2_score_base_train = 1/f2(y_train.ravel(), LRP_train)
    f2_score_base_test = 1/f2(y_test.ravel(), LRP_test)
    f2_score_train = 1/f2(y_train.ravel(), y_pred_train.ravel())
    f2_score_test = 1/f2(y_test.ravel(), y_pred_test.ravel())
        
    # Mean Squared Error
    mse_base_train = msehr(y_train.ravel(), LRP_train)
    mse_base_test = msehr(y_test.ravel(), LRP_test)
    mse_train = msehr(y_train.ravel(), y_pred_train.ravel())
    mse_test = msehr(y_test.ravel(), y_pred_test.ravel())
        
    # Final Score/Loss
    loss_base_train = L(y_train.ravel(), LRP_train)
    loss_base_test = L(y_test.ravel(), LRP_test)
    loss_train = L(y_train.ravel(), y_pred_train.ravel())
    loss_test = L(y_test.ravel(), y_pred_test.ravel())    

    #Plot results
    axs[0].plot(mse_train, mse_test, '.', color=cmap(kk), label='Model Fold %d'%(kk+1))
    axs[0].plot([mse_base_train]*2, [0,1], '--', color=cmap(kk), label='Baseline Fold %d'%(kk+1))
    axs[0].plot([0,0.8], [mse_base_test]*2, '--', color=cmap(kk))

    axs[1].plot(f2_score_train, f2_score_test, '.', color=cmap(kk), label='Model Fold %d'%(kk+1))
    axs[1].plot([f2_score_base_train]*2, [1,4], '--', color=cmap(kk), label='Baseline Fold %d'%(kk+1))
    axs[1].plot([1,3], [f2_score_base_test]*2, '--', color=cmap(kk))

    axs[2].plot(loss_train, loss_test, '.', color=cmap(kk), label='Model Fold %d'%(kk+1))
    axs[2].plot([loss_base_train]*2, [0,2.5], '--', color=cmap(kk), label='Baseline Fold %d'%(kk+1))
    axs[2].plot([0,2], [loss_base_test]*2, '--', color=cmap(kk))


    # Print results    
    print('{:<10}{:<10}{:<10}{:<10}'.format('','1/F2','MSE','Loss'))
    print('{:<10}{:<10.5}{:<10.5}{:<10.5}'.format('LRP Train',f2_score_base_train, mse_base_train, loss_base_train))
    print('{:<10}{:<10.5}{:<10.5}{:<10.5}'.format('LRP Test',f2_score_base_test, mse_base_test, loss_base_test))
    print('{:<10}{:<10.5}{:<10.5}{:<10.5}'.format('Train',f2_score_train, mse_train, loss_train))
    print('{:<10}{:<10.5}{:<10.5}{:<10.5}'.format('Test',f2_score_test, mse_test, loss_test))
    print('LRP Train Confusion Matrix')
    print(confusion_matrix(y_clf(y_train), y_clf(LRP_train)))
    print('LRP Train Classification Report')
    print(classification_report(y_clf(y_train), y_clf(LRP_train)))
    print('LRP Test Confusion Matrix')
    print(confusion_matrix(y_clf(y_test), y_clf(LRP_test)))
    print('LRP Test Classification Report')
    print(classification_report(y_clf(y_test), y_clf(LRP_test)))
    print('Train Confusion Matrix')
    print(confusion_matrix(y_clf(y_train), y_pred_trainclf))
    print('Train Classification Report')
    print(classification_report(y_clf(y_train), y_pred_trainclf))
    print('Test Confusion Matrix')
    print(confusion_matrix(y_clf(y_test), y_pred_testclf))
    print('Test Classification Report')
    print(classification_report(y_clf(y_test), y_pred_testclf))



  axs[0].set_xlabel('Train set MSE')
  axs[0].set_ylabel('Test set MSE')
  # axs[0].legend()

  axs[1].set_xlabel('Train set 1/F2')
  axs[1].set_ylabel('Test set 1/F2')
  # axs[1].legend()

  axs[2].set_xlabel('Train set loss')
  axs[2].set_ylabel('Test set loss')
  # axs[2].legend()

  plt.show()