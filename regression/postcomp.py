import pandas as pd
import os
import numpy as np
from sklearn import preprocessing
from sklearn.model_selection import KFold, StratifiedKFold
import lightgbm
from sklearn.metrics import confusion_matrix, fbeta_score, precision_score
from matplotlib import pyplot as plt
from sklearn.utils import shuffle

from binning import df_preprocess, y_clf, msehr, f2, L

THRESHOLD = -6

if __name__=='__main__':
  # Read the training data
  path_to_dataset = os.path.join(os.path.dirname(__file__), '..', 'data')
  train = pd.read_csv(os.path.join(path_to_dataset,'train_data.csv'))
  #test = pd.read_csv(os.path.join(path_to_dataset,'test_data.csv'))

  df = df_preprocess(train)

  # clipping at a risk of -6.001
  df.loc[df['target_risk']<THRESHOLD,'target_risk']=THRESHOLD-10**(-3)
  df.loc[df['risk']<THRESHOLD,'risk']=THRESHOLD-10**(-3)

  # the model’s target was defined to be the change in
  # risk value between the input CDM and the event’s final CDM. 
  # It allows for a direct comparison to the LRP baseline as 
  # the model predicts a new estimator h such that r = LRP + h.
  df['target'] = df['target_risk'] - df['risk']

  df['risk_class'] = 0
  df['risk_class'].where(df['target_risk']>=THRESHOLD, 1, inplace=True)

  # Data split
  X = df.drop(columns=[
      'mission_id',
      'c_object_type',
      #'risk',
      'target_risk',
      #'target'
  ])
  y = df['risk_class'].copy()

  fig, axs = plt.subplots(ncols=3, figsize=(15,5))
  cmap = plt.get_cmap("tab10")

  kf = StratifiedKFold(n_splits=3, random_state=1)
  for kk , (train_index, test_index) in enumerate(kf.split(X, y)):
    X_train, X_test = X.iloc[list(train_index)], X.iloc[list(test_index)]
    y_train, y_test = y.iloc[list(train_index)], y.iloc[list(test_index)]
    X_train, y_test = shuffle(X_train, y_train)

    risk_train = X_train['risk']
    risk_test = X_test['risk']
    y_train = X_train['target']
    y_test = X_test['target']
    X_train.drop(columns=['risk','target'], inplace=True)
    X_test.drop(columns=['risk','target'], inplace=True)
    
    
    #h (target) was encoded through a quantile transformer to assume a uniform distribution
    # qt = preprocessing.QuantileTransformer(
    #   #n_quantiles=X_train.shape[0], 
    #   random_state=0
    # )
    # y_train = qt.fit_transform(y_train.values.reshape(-1, 1))
    #y_test = qt.transform(y_test.values.reshape(-1, 1))
    
    #LGBMRegressor
    model = lightgbm.LGBMRegressor(
      learning_rate=0.05,
      n_estimators=25,
      feature_fraction=0.25
    )
    
    model.fit(X_train, y_train.ravel())
    y_pred_train = model.predict(X_train)
    y_pred_test = model.predict(X_test)
    
    # y_train = qt.inverse_transform(y_train.reshape(-1, 1))
    # y_pred_train = qt.inverse_transform(y_pred_train.reshape(-1, 1))
    # y_pred_test = qt.inverse_transform(y_pred_test.reshape(-1, 1))
    
    print('------------ %2d ------------' % (kk+1))
    #plt.plot(y_test.ravel()+risk_test.values, y_pred.ravel()+risk_test.values, '.')
    #plt.plot(y_test.ravel(), y_pred.ravel(), '.')
    

    # Baseline
    LRP_train = np.where(risk_train.values>=THRESHOLD, risk_train.values, THRESHOLD-10**(-3))
    LRP_test = np.where(risk_test.values>=THRESHOLD, risk_test.values, THRESHOLD-10**(-3))

    
    # 1/F2 Score
    f2_score_base_train = 1/f2(y_train.ravel()+risk_train.values, LRP_train)
    f2_score_base_test = 1/f2(y_test.ravel()+risk_test.values, LRP_test)
    f2_score_train = 1/f2(y_train.ravel()+risk_train.values, y_pred_train.ravel()+risk_train.values)
    f2_score_test = 1/f2(y_test.ravel()+risk_test.values, y_pred_test.ravel()+risk_test.values)
    
    # Mean Squared Error
    mse_base_train = msehr(y_train.ravel()+risk_train.values, LRP_train) 
    mse_base_test = msehr(y_test.ravel()+risk_test.values, LRP_test)
    mse_train = msehr(y_train.ravel()+risk_train.values, y_pred_train.ravel()+risk_train.values)
    mse_test = msehr(y_test.ravel()+risk_test.values, y_pred_test.ravel()+risk_test.values)
    
    # Final Score/Loss
    loss_base_train = L(y_train.ravel()+risk_train.values, LRP_train)
    loss_base_test = L(y_test.ravel()+risk_test.values, LRP_test)
    loss_train = L(y_train.ravel()+risk_train.values, y_pred_train.ravel()+risk_train.values)
    loss_test = L(y_test.ravel()+risk_test.values, y_pred_test.ravel()+risk_test.values)

    
    #Plot results
    axs[0].plot(mse_train, mse_test, '.', color=cmap(kk), label='Model Fold %d'%(kk+1))
    axs[0].plot([mse_base_train]*2, [0,1], '--', color=cmap(kk), label='Baseline Fold %d'%(kk+1))
    axs[0].plot([0,0.8], [mse_base_test]*2, '--', color=cmap(kk))

    axs[1].plot(f2_score_train, f2_score_test, '.', color=cmap(kk), label='Model Fold %d'%(kk+1))
    axs[1].plot([f2_score_base_train]*2, [1,4], '--', color=cmap(kk), label='Baseline Fold %d'%(kk+1))
    axs[1].plot([1,3], [f2_score_base_test]*2, '--', color=cmap(kk))

    axs[2].plot(loss_train, loss_test, '.', color=cmap(kk), label='Model Fold %d'%(kk+1))
    axs[2].plot([loss_base_train]*2, [0,2.5], '--', color=cmap(kk), label='Baseline Fold %d'%(kk+1))
    axs[2].plot([0,2], [loss_base_test]*2, '--', color=cmap(kk))


    # Print results    
    print('{:<10}{:<10}{:<10}{:<10}'.format('','1/F2','MSE','Loss'))
    print('{:<10}{:<10.5}{:<10.5}{:<10.5}'.format('LRP Train',f2_score_base_train, mse_base_train, loss_base_train))
    print('{:<10}{:<10.5}{:<10.5}{:<10.5}'.format('LRP Test',f2_score_base_test, mse_base_test, loss_base_test))
    print('{:<10}{:<10.5}{:<10.5}{:<10.5}'.format('Train',f2_score_train, mse_train, loss_train))
    print('{:<10}{:<10.5}{:<10.5}{:<10.5}'.format('Test',f2_score_test, mse_test, loss_test))
    # print('LRP Train Confusion Matrix')
    # print(confusion_matrix(y_clf(y_train), y_clf(LRP_train)))
    # print('LRP Test Confusion Matrix')
    # print(confusion_matrix(y_clf(y_test), y_clf(LRP_test)))
    # print('Train Confusion Matrix')
    # print(confusion_matrix(y_clf(y_train), y_pred_trainclf))
    # print('Test Confusion Matrix')
    # print(confusion_matrix(y_clf(y_test), y_pred_testclf))

    
  axs[0].set_xlabel('Train set MSE')
  axs[0].set_ylabel('Test set MSE')
  axs[0].set_title('MSE')
  #axs[0].legend()

  axs[1].set_xlabel('Train set 1/F2')
  axs[1].set_ylabel('Test set 1/F2')
  axs[1].set_title('1/F2')
  #axs[1].legend()

  axs[2].set_xlabel('Train set loss')
  axs[2].set_ylabel('Test set loss')
  axs[2].set_title('Loss')
  axs[2].legend()

  plt.show()
